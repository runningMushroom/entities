# Functionality
* This Entities Library is inteded to be the base from which every application can be built further, It will not only be referenced but rather copied in & edited as need be
* Base:
	* BaseModals:
		* BaseModal from which every other modal will inherit, this ensures created & modified dates are update on create & update
		* BaseUser that serves as a base for all user classes
	* IRepo & BaseRepo
		* Base Repository functionality - Repository structure
* DTO - Dto classes for:
	* User Authentication
	* Connection String
	* Jwt Settings
	* User Registration
	* Users
* Interfaces & Repositories
	* JwtRepo creates user JWT Tokens, placed here because it requires the user models.  Placed in it's own repo so that it can be used for all user classes
	* Users & System Users inherit from IBaseRepo & BaseRepo
* Models
	* Users & System Users inherit from base user and can be extended further if required
* Context - DbContext class
* IRepoWrapper & RepoWrapper


# Target Framework
* netcoreapp3.1

# Instructions
* In Entities.Extensions.cs change appName to your Main applications name:
	* services.AddDbContext<Context>(o => o.UseSqlServer(connectionString.ConnectionString, b => b.MigrationsAssembly("appName")));
* Add DefaultSqlConnection to your appsettings.json
	* 
* Add Jwt Token paramaters to appsettings.json
	*
* add to your Startup.cs file:
	* ConfigureServices
		* services.ConfigureEntities(Configuration)
	* Configure
		* app.UseAuthentication()
		* app.UseAuthorization()