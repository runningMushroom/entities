﻿using Entities.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public interface IRepoWrapper
    {
        IUserRepo User { get; }
        ISystemUserRepo SystemUser { get; }

        void Save();
    }
}