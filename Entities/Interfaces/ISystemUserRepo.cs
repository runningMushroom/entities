﻿using Entities.Base;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Interfaces
{
    public interface ISystemUserRepo : IBaseRepo<SystemUserModel>
    {
    }
}