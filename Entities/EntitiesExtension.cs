﻿using Entities.Dto;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public static class EntitiesExtension
    {
        public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration config)
        {
            // options.UseSqlServer(connection, b => b.MigrationsAssembly("alumaV2")).
            var connectionString = config.GetSection("DefaultSqlConnection").Get<ContextDto>();

            services.AddDbContext<Context>(o => o.UseSqlServer(connectionString.ConnectionString, b => b.MigrationsAssembly("appName")));

            services.AddScoped<IRepoWrapper, RepoWrapper>();

            services.AddSingleton(config.GetSection("JwtSettings").Get<JwtSettingsDto>());
        }
    }
}