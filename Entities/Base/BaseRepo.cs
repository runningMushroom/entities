﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Entities.Base
{
    public abstract class BaseRepo<T> : IBaseRepo<T> where T : class
    {
        protected Context _context { get; set; }

        public BaseRepo(Context context)
        {
            _context = context;
        }

        public IQueryable<T> FindAll()
        {
            return _context.Set<T>().AsNoTracking();
        }

        public IQueryable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return _context.Set<T>().Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            entity.GetType().GetProperty("Created").SetValue(entity, DateTime.Now);
            entity.GetType().GetProperty("Modified").SetValue(entity, DateTime.Now);
            _context.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            entity.GetType().GetProperty("Modified").SetValue(entity, DateTime.Now);
            _context.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            _context.Set<T>().Remove(entity);
        }
    }
}