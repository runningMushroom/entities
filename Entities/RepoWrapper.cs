﻿using Entities.Dto;
using Entities.Interfaces;
using Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class RepoWrapper : IRepoWrapper
    {
        private Context _context;
        private IJwtRepo _jwtRepo;
        private JwtSettingsDto _jwtSettings;
        private IUserRepo _user;
        private ISystemUserRepo _systemUser;

        public RepoWrapper(
            Context context,
            IJwtRepo jwtRepo,
            JwtSettingsDto jwtSettings,
            IUserRepo user,
            ISystemUserRepo systemUser)
        {
            _context = context;
            _jwtRepo = jwtRepo;
            _jwtSettings = jwtSettings;
            _user = user;
            _systemUser = systemUser;
        }

        public IJwtRepo JwtRepo
        {
            get { return _jwtRepo == null ? new JwtRepo(_jwtSettings) : _jwtRepo; }
        }

        public IUserRepo User
        {
            get { return _user == null ? new UserRepo(_context) : _user; }
        }

        public ISystemUserRepo SystemUser
        {
            get { return _systemUser == null ? new SystemUserRepo(_context) : _systemUser; }
        }

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}