﻿using Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace Entities
{
    public class Context : DbContext
    {
        public Context(DbContextOptions options) : base(options)
        {
        }

        public DbSet<UserModel> Users { get; set; }
        public DbSet<SystemUserModel> SystemUsers { get; set; }
    }
}