﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Dto
{
    public class ContextDto
    {
        public string ConnectionString { get; set; }
    }
}