﻿using Entities.Base;
using Entities.Interfaces;
using Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Repositories
{
    public class SystemUserRepo : BaseRepo<SystemUserModel>, ISystemUserRepo
    {
        public SystemUserRepo(Context _context) : base(_context)
        {
        }
    }
}